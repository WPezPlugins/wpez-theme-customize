<?php
/*
Plugin Name: WPezPlugins: Theme Customize: TODO
Plugin URI: https://gitlab.com/WPezPlugins/wpez-theme-customize
Description: Boilerplate for customizations of your theme / parent theme.
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: TODO
*/

namespace WPezThemeCustomize\App;

use WPezThemeCustomize\App\Core\HooksRegister\ClassHooksRegister;

use WPezThemeCustomize\App\Theme\Actions\ClassActions;
use WPezThemeCustomize\App\Theme\Filters\ClassFilters;
use WPezThemeCustomize\App\Theme\Other\ClassOther;
use WPezThemeCustomize\App\Theme\WPCore\ClassWPCore;


class ClassPlugin {

    protected $_new_hooks_reg;

    protected $_arr_actions;

    protected $_arr_filters;

    protected $_new_wpcore;


    public function __construct() {

        $this->setPropertyDefaults();

        $this->actions( false );

        $this->filters( false );

        $this->other( false );

        $this->wpCore( false );

        // this should be last
        $this->hooksRegister();

    }

    protected function setPropertyDefaults() {

        $this->_new_hooks_reg = new ClassHooksRegister();
        $this->_arr_actions   = [];
        $this->_arr_filters   = [];

        $this->_new_wpcore = new ClassWPCore();
    }

    /**
     * After gathering (below) the arr_actions and arr_filter, it's time to
     * make some RegisterHook magic
     */
    protected function hooksRegister() {

        $this->_new_hooks_reg->loadActions( $this->_arr_actions );

        $this->_new_hooks_reg->loadFilters( $this->_arr_filters );

        $this->_new_hooks_reg->doRegister();

    }


    public function actions( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $new_actions = new ClassActions();

        $this->_arr_actions[] = [
            'active'    => false, // active does not have to be set. it - in ClassRegisterHooks - is default: true
            'hook'      => 'init',
            'component' => $new_actions,
            'callback'  => 'parentActionExampleAction',
            // 'priority' => 75
        ];

    }

    public function filters( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $new_filters = new ClassFilters();

        $this->_arr_filters[] = [
            'hook'      => 'twentyseventeen_custom_header_args',
            'component' => $new_filters,
            'callback'  => 'parentFilterExampleFilter',
            // 'priority' => 75
        ];
    }


    public function other( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $new_other = new ClassOther();
    }


    public function wpCore( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $this->wpDequeueStyle( false );

        $this->wpDequeueScript( false );

        $this->deregisterPostType( false );

        $this->postTypeSupport( false );

        $this->unregisterSidebar( false );

        $this->unregisterWidget( false );

        $this->removeSetting( false );
    }


    protected function wpDequeueStyle( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $arr_args = [
            // 'style-name-1' => true, // true = remove
            // 'style-name-2' => false // false = leave
        ];
        $this->_new_wpcore->setStyles( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'wp_enqueue_scripts',
            'component' => $this->_new_wpcore,
            'callback'  => 'wpDequeueStyle',
            'priority'  => 100
        ];
    }


    protected function wpDequeueScript( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $arr_args = [
            // 'script-name-1' => true,
            // 'script-name-2' => false
        ];
        $this->_new_wpcore->setScripts( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'wp_enqueue_scripts',
            'component' => $this->_new_wpcore,
            'callback'  => 'wpDequeueScript',
            'priority'  => 100
        ];
    }


    protected function deregisterPostType( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $arr_args = [
            // 'post-type-1' => true,
            // 'post-type-2' => false
        ];
        $this->_new_wpcore->setPostTypes( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'init',
            'component' => $this->_new_wpcore,
            'callback'  => 'deregisterPostType',
            'priority'  => 100
        ];
    }


    protected function postTypeSupport( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $arr_args = [
            // 'support-1' => true,
            // 'support-2' => false
        ];
        $this->_new_wpcore->setPostTypesSupport( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'init',
            'component' => $this->_new_wpcore,
            'callback'  => 'removePostTypeSupport',
            'priority'  => 100
        ];

        $this->_arr_actions[] = [
            'hook'      => 'init',
            'component' => $this->_new_wpcore,
            'callback'  => 'addPostTypeSupport',
            'priority'  => 105
        ];
    }


    protected function unregisterSidebar( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $arr_args = [
            // 'sidebar-name-1' => true,
            // 'sidebar-name-2' => false
        ];
        $this->_new_wpcore->setSidebars( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'init',
            'component' => $this->_new_wpcore,
            'callback'  => 'unregisterSidebar',
            'priority'  => 100
        ];

    }


    protected function unregisterWidget( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $arr_args = [
            // 'widget-name-1' => true,
            // 'widget-name-2' => false
        ];
        $this->_new_wpcore->setWidgets( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'init',
            'component' => $this->_new_wpcore,
            'callback'  => 'unregisterWidget',
            'priority'  => 100
        ];

    }

    protected function removeSetting( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        // Don't forget to use set_theme_mod() to "replace" the values for the settings you remove
        $arr_args = [
            // 'setting-name-1' => true,
            // 'setting-name-2' => false
        ];
        $this->_new_wpcore->setSettings( $arr_args );

        $this->_arr_actions[] = [
            'hook'      => 'customize_register',
            'component' => $this->_new_wpcore,
            'callback'  => 'removeSetting',
            'priority'  => 100
        ];

    }
}